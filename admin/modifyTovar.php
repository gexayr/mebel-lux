<?php
require "../pdo/common.php";
require "../pdo/config.php";
require "header.php";

if(!empty($_GET['tovar'])) {
    $sliderImg = [];
    $sql = "SELECT * FROM product where id = :id";
    $statement = $connection->prepare($sql);
    $statement->bindParam(':id', $_GET['tovar'], PDO::PARAM_INT);
    $statement->execute();
    $result = $statement->fetchAll();
    if (!empty($result)) {
        $tovarLol = $result[0];
        $productId = $tovarLol['id'];
        $productName=$tovarLol['productName'];
        $productImg=$tovarLol['productImg'];
        $productDescr=$tovarLol['productDescr'];
        $productStock=$tovarLol['stock'];
        $price=$tovarLol['price'];
        $category = $tovarLol['category'];

        $sql = "SELECT * FROM product_images
        where product_id = :id";
        $statement = $connection->prepare($sql);
        $statement->bindParam(':id', $productId, PDO::PARAM_INT);
        $statement->execute();
        $result = $statement->fetchAll();

        $productImg = $result;
        $sliderImg = $productImg;
    }
}
$pages = $connection->query("SELECT * FROM mainPage");

if (!empty($_POST['newName'])) {
    $newName = $_POST['newName'];
    $connection->query("UPDATE product SET productName='$newName' where id='$productId'");
    header("Location:modifyTovar.php?tovar=$productId");
}
if (!empty($_POST['newDescr'])) {
    $newDescr = htmlspecialchars_decode($_POST['newDescr']);
    $connection->query("UPDATE potolkiv_mebel.product SET productDescr='$newDescr' where id='$productId'");
    header("Location:modifyTovar.php?tovar=$productId");
}
if (!empty($_POST['newPrice'])) {
    $newPrice = $_POST['newPrice'];
    $connection->query("UPDATE potolkiv_mebel.product SET price='$newPrice' where id='$productId'");
    header("Location:modifyTovar.php?tovar=$productId");
}
if (isset($_POST['submitLol'])) {

//    $imgSrc = '../'.$productImg;
//    unlink($imgSrc);
    $goodExtension = ['jpg', 'jpeg', 'png'];
    $newProduct = $_POST['tovarName'];
    $productCategory = $_POST['tovarCategory'];
    $productDescr = htmlspecialchars_decode($_POST['tovarDescr']);
    $productPrice = $_POST['tovarPrice'];

    $nameForBd = 'images/default.png';
    $connection->query("INSERT INTO potolkiv_mebel.product 
        (productName, productImg, productDescr,price,category) VALUE 
        ('$newProduct','$nameForBd','$productDescr','$productPrice','$productCategory')");

    foreach ($_FILES['file']['name'] as $key=>$value) {

        $fileName = $value;
        $fileTmpName = $_FILES['file']['tmp_name'][$key];
        $fileType = $_FILES['file']['type'][$key];
        $fileError = $_FILES['file']['error'][$key];
        $fileSize = $_FILES['file']['size'][$key];

        $fileExtension = strtolower(end(explode('.', $fileName)));
        if (in_array($fileExtension, $goodExtension)) {
            if ($fileSize < 500000) {
                if ($fileError == 0) {
                    $fileNameNew = time() . $fileName;
                    $nameForBd = 'images/' . $fileNameNew;
                    $connection->query("INSERT INTO product_images (name, product_id) VALUE ('$nameForBd','$productId')");
                    $fileDestination = '../images/' . $fileNameNew;
                    move_uploaded_file($fileTmpName, $fileDestination);
                } else {
                    echo 'Что-то пошло не так';
                }
            } else {
                echo 'Слишком большой файл';
            }
        } else {
            echo 'Неверный тип файла';
        }
    }

    header("Location:modifyTovar.php?tovar=$productId");
}

if (isset($_POST['submit'])) {
    $productStock = $_POST['stock'] ? $_POST['stock'] : 0;
    $connection->query("UPDATE potolkiv_mebel.product SET stock='$productStock' where id='$productId'");
    header("Location:modifyTovar.php?tovar=$productId");
}

?>

  <!-- Bootstrap core CSS -->

    <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500,700" rel="stylesheet">

    <link rel="stylesheet" href="../css/open-iconic-bootstrap.min.css">
    <link rel="stylesheet" href="../css/animate.css">

    <link rel="stylesheet" href="../css/owl.carousel.min.css">
    <link rel="stylesheet" href="../css/owl.theme.default.min.css">
    <link rel="stylesheet" href="../css/magnific-popup.css">

    <link rel="stylesheet" href="../css/aos.css">

    <link rel="stylesheet" href="../css/ionicons.min.css">

    <link rel="stylesheet" href="../css/bootstrap-datepicker.css">
    <link rel="stylesheet" href="../css/jquery.timepicker.css">


    <link rel="stylesheet" href="../css/flaticon.css">
    <link rel="stylesheet" href="../css/icomoon.css">

    <link rel="stylesheet" href="../css/tovar.css">
    <link href="../css/bootstrap.css" rel="stylesheet" type="text/css" media="all">

  <link href="css/bootstrap.min.css" rel="stylesheet">

  <link href="fonts/css/font-awesome.min.css" rel="stylesheet">
  <link href="css/animate.min.css" rel="stylesheet">

  <!-- Custom styling plus plugins -->
  <link href="css/custom.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="css/maps/jquery-jvectormap-2.0.3.css" />
  <link href="css/icheck/flat/green.css" rel="stylesheet" />
  <link href="css/floatexamples.css" rel="stylesheet" type="text/css" />

  <script src="js/jquery.min.js"></script>
  <script src="js/nprogress.js"></script>

  <!--[if lt IE 9]>
        <![endif]-->

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    <script src="https://cdn.ckeditor.com/4.14.0/standard/ckeditor.js"></script>

</head>


<body class="nav-md">

  <div class="container body">


    <div class="main_container">

        <?php
        require "sidebar.php";
        require "navigation.php";
        ?>

      <!-- page content -->
        <div class="right_col" role="main">
        <div class="page">
                <div class="product col-sm-12" style="background: white;display: flex;flex-wrap: wrap;">
                    <div class="product_img col-sm-6 col-xs-12" style="display: flex;flex-direction: column;padding: 10%;">
                        <div style="overflow: visible;" class="fotorama" data-loop="true" data-width="300" data-nav="thumbs">
                            <?foreach ($productImg as $sliderImgLol) {?>
                                <img width="100" src="../<?=$sliderImgLol['name']?>" alt="">
                            <?}?>
                        </div>
                        <form action="" method="POST" enctype="multipart/form-data">
                            <label for="">
                                <span>Изменить картинку</span>
                                <input type="file" name="file[]" multiple>
                            </label>
                            <button name="submitLol">Изменить</button>
                        </form>
                      
                    </div>
                    <div class="product_descr col-sm-6 col-xs-12">
                        <h4>Название товара</h4>
                        <p> <?=$productName?></p>
                        <form action="" method="post">
                            <input type="text" name="newName" placeholder="Новое название" required>
                            <button name = "submit">Изменить</button>
                        </form>
                        <form action="" method="post">
                                <span class="span-akcia">Товар по акции</span>
                                <input type="checkbox" name="stock" class="checkbox-akcia" value="1" <?=$productStock==1 ? 'checked' : ''?>>
                            <button name ="submit">Изменить</button>
                        </form>
                        <h4>Краткое описание</h4>
                        <form action="" method="post">
                            <textarea style="width: 100%;border: 2px solid red;" name="newDescr" required><?=$productDescr?></textarea><br><br>
                            <button name ="submit">Изменить</button>
                        </form>
                        <h5>Стоимость: <span><?=$price?> р.</span></h5>
                        <form action="" method="post">
                            <input type="text" name="newPrice" placeholder="Новая цена" required>
                            <button name ="submit">Изменить</button>
                        </form>
                    </div>
                </div>
        </div>
        </div>
      <!-- /page content -->

    </div>

  </div>

  <div id="custom_notifications" class="custom-notifications dsp_none">
    <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
    </ul>
    <div class="clearfix"></div>
    <div id="notif-group" class="tabbed_notifications"></div>
  </div>

  <script src="js/bootstrap.min.js"></script>

  <!-- gauge js -->
  <script type="text/javascript" src="js/gauge/gauge.min.js"></script>
  <script type="text/javascript" src="js/gauge/gauge_demo.js"></script>
  <!-- bootstrap progress js -->
  <script src="js/progressbar/bootstrap-progressbar.min.js"></script>
  <script src="js/nicescroll/jquery.nicescroll.min.js"></script>
  <!-- icheck -->
  <script src="js/icheck/icheck.min.js"></script>
  <!-- daterangepicker -->
  <script type="text/javascript" src="js/moment/moment.min.js"></script>
  <script type="text/javascript" src="js/datepicker/daterangepicker.js"></script>
  <!-- chart js -->
  <script src="js/chartjs/chart.min.js"></script>

  <script src="js/custom.js"></script>

  <!-- flot js -->
  <!--[if lte IE 8]><script type="text/javascript" src="js/excanvas.min.js"></script><![endif]-->
  <script type="text/javascript" src="js/flot/jquery.flot.js"></script>
  <script type="text/javascript" src="js/flot/jquery.flot.pie.js"></script>
  <script type="text/javascript" src="js/flot/jquery.flot.orderBars.js"></script>
  <script type="text/javascript" src="js/flot/jquery.flot.time.min.js"></script>
  <script type="text/javascript" src="js/flot/date.js"></script>
  <script type="text/javascript" src="js/flot/jquery.flot.spline.js"></script>
  <script type="text/javascript" src="js/flot/jquery.flot.stack.js"></script>
  <script type="text/javascript" src="js/flot/curvedLines.js"></script>
  <script type="text/javascript" src="js/flot/jquery.flot.resize.js"></script>
  <script>
    $(document).ready(function() {
      // [17, 74, 6, 39, 20, 85, 7]
      //[82, 23, 66, 9, 99, 6, 2]
      var data1 = [
        [gd(2012, 1, 1), 17],
        [gd(2012, 1, 2), 74],
        [gd(2012, 1, 3), 6],
        [gd(2012, 1, 4), 39],
        [gd(2012, 1, 5), 20],
        [gd(2012, 1, 6), 85],
        [gd(2012, 1, 7), 7]
      ];

      var data2 = [
        [gd(2012, 1, 1), 82],
        [gd(2012, 1, 2), 23],
        [gd(2012, 1, 3), 66],
        [gd(2012, 1, 4), 9],
        [gd(2012, 1, 5), 119],
        [gd(2012, 1, 6), 6],
        [gd(2012, 1, 7), 9]
      ];
      $("#canvas_dahs").length && $.plot($("#canvas_dahs"), [
        data1, data2
      ], {
        series: {
          lines: {
            show: false,
            fill: true
          },
          splines: {
            show: true,
            tension: 0.4,
            lineWidth: 1,
            fill: 0.4
          },
          points: {
            radius: 0,
            show: true
          },
          shadowSize: 2
        },
        grid: {
          verticalLines: true,
          hoverable: true,
          clickable: true,
          tickColor: "#d5d5d5",
          borderWidth: 1,
          color: '#fff'
        },
        colors: ["rgba(38, 185, 154, 0.38)", "rgba(3, 88, 106, 0.38)"],
        xaxis: {
          tickColor: "rgba(51, 51, 51, 0.06)",
          mode: "time",
          tickSize: [1, "day"],
          //tickLength: 10,
          axisLabel: "Date",
          axisLabelUseCanvas: true,
          axisLabelFontSizePixels: 12,
          axisLabelFontFamily: 'Verdana, Arial',
          axisLabelPadding: 10
            //mode: "time", timeformat: "%m/%d/%y", minTickSize: [1, "day"]
        },
        yaxis: {
          ticks: 8,
          tickColor: "rgba(51, 51, 51, 0.06)",
        },
        tooltip: false
      });

      function gd(year, month, day) {
        return new Date(year, month - 1, day).getTime();
      }
    });
  </script>

  <!-- worldmap -->
  <script type="text/javascript" src="js/maps/jquery-jvectormap-2.0.3.min.js"></script>
  <script type="text/javascript" src="js/maps/gdp-data.js"></script>
  <script type="text/javascript" src="js/maps/jquery-jvectormap-world-mill-en.js"></script>
  <script type="text/javascript" src="js/maps/jquery-jvectormap-us-aea-en.js"></script>
  <!-- pace -->
  <script src="js/pace/pace.min.js"></script>
  <script>
      CKEDITOR.replace( 'newDescr');
  </script>
  <script>
    $(function() {
      $('#world-map-gdp').vectorMap({
        map: 'world_mill_en',
        backgroundColor: 'transparent',
        zoomOnScroll: false,
        series: {
          regions: [{
            values: gdpData,
            scale: ['#E6F2F0', '#149B7E'],
            normalizeFunction: 'polynomial'
          }]
        },
        onRegionTipShow: function(e, el, code) {
          el.html(el.html() + ' (GDP - ' + gdpData[code] + ')');
        }
      });
    });
  </script>
  <!-- skycons -->
  <script src="js/skycons/skycons.min.js"></script>
  <script>
    var icons = new Skycons({
        "color": "#73879C"
      }),
      list = [
        "clear-day", "clear-night", "partly-cloudy-day",
        "partly-cloudy-night", "cloudy", "rain", "sleet", "snow", "wind",
        "fog"
      ],
      i;

    for (i = list.length; i--;)
      icons.set(list[i], list[i]);

    icons.play();
  </script>

  <!-- dashbord linegraph -->
  <script>
    Chart.defaults.global.legend = {
      enabled: false
    };

    var data = {
      labels: [
        "Symbian",
        "Blackberry",
        "Other",
        "Android",
        "IOS"
      ],
      datasets: [{
        data: [15, 20, 30, 10, 30],
        backgroundColor: [
          "#BDC3C7",
          "#9B59B6",
          "#455C73",
          "#26B99A",
          "#3498DB"
        ],
        hoverBackgroundColor: [
          "#CFD4D8",
          "#B370CF",
          "#34495E",
          "#36CAAB",
          "#49A9EA"
        ]

      }]
    };

    var canvasDoughnut = new Chart(document.getElementById("canvas1"), {
      type: 'doughnut',
      tooltipFillColor: "rgba(51, 51, 51, 0.55)",
      data: data
    });
  </script>
  <!-- /dashbord linegraph -->
  <!-- datepicker -->
  <script type="text/javascript">
    $(document).ready(function() {

      var cb = function(start, end, label) {
        console.log(start.toISOString(), end.toISOString(), label);
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        //alert("Callback has fired: [" + start.format('MMMM D, YYYY') + " to " + end.format('MMMM D, YYYY') + ", label = " + label + "]");
      }

      var optionSet1 = {
        startDate: moment().subtract(29, 'days'),
        endDate: moment(),
        minDate: '01/01/2012',
        maxDate: '12/31/2015',
        dateLimit: {
          days: 60
        },
        showDropdowns: true,
        showWeekNumbers: true,
        timePicker: false,
        timePickerIncrement: 1,
        timePicker12Hour: true,
        ranges: {
          'Today': [moment(), moment()],
          'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days': [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month': [moment().startOf('month'), moment().endOf('month')],
          'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        opens: 'left',
        buttonClasses: ['btn btn-default'],
        applyClass: 'btn-small btn-primary',
        cancelClass: 'btn-small',
        format: 'MM/DD/YYYY',
        separator: ' to ',
        locale: {
          applyLabel: 'Submit',
          cancelLabel: 'Clear',
          fromLabel: 'From',
          toLabel: 'To',
          customRangeLabel: 'Custom',
          daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
          monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
          firstDay: 1
        }
      };
      $('#reportrange span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));
      $('#reportrange').daterangepicker(optionSet1, cb);
      $('#reportrange').on('show.daterangepicker', function() {
        console.log("show event fired");
      });
      $('#reportrange').on('hide.daterangepicker', function() {
        console.log("hide event fired");
      });
      $('#reportrange').on('apply.daterangepicker', function(ev, picker) {
        console.log("apply event fired, start/end dates are " + picker.startDate.format('MMMM D, YYYY') + " to " + picker.endDate.format('MMMM D, YYYY'));
      });
      $('#reportrange').on('cancel.daterangepicker', function(ev, picker) {
        console.log("cancel event fired");
      });
      $('#options1').click(function() {
        $('#reportrange').data('daterangepicker').setOptions(optionSet1, cb);
      });
      $('#options2').click(function() {
        $('#reportrange').data('daterangepicker').setOptions(optionSet2, cb);
      });
      $('#destroy').click(function() {
        $('#reportrange').data('daterangepicker').remove();
      });
    });
  </script>
  <script>
    NProgress.done();
  </script>
  <!-- /datepicker -->
  <!-- /footer content -->
    <script src="../script.js"></script>
</body>

</html>
