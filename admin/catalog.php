<?php
require "../pdo/common.php";
require "../pdo/config.php";
require "header.php";

define("ROW_PER_PAGE",30);

$pages = $connection->query("SELECT * FROM potolkiv_mebel.mainPage");

//pagination_part
if (isset($_GET['pageno'])) {
    $pageno = $_GET['pageno'];
} else {
    $pageno = 1;
}
/* Pagination Code starts */
$sql = 'SELECT * FROM catalog ORDER BY id DESC ';
$per_page_html = '';
$page = 1;
$start=0;
if(!empty($_GET["page_number"])) {
    $page = $_GET["page_number"];
    $start=($page-1) * ROW_PER_PAGE;
}


$limit=" limit " . $start . "," . ROW_PER_PAGE;
$pagination_statement = $connection->prepare($sql);
$pagination_statement->execute();

$row_count = $pagination_statement->rowCount();
if(!empty($row_count)){
    $per_page_html .= "<div style='text-align:center;margin:20px 0px;'>";
    $page_count=ceil($row_count/ROW_PER_PAGE);
    if($page_count>1) {
        for($i=1;$i<=$page_count;$i++){
            if($i==$page){
                $per_page_html .= '<span class="btn-page current btn-dark">' . $i . '</span>';
            } else {
                $per_page_html .= '<a href="?page_number=' . $i . '" class="btn-page">' . $i . '</a>';
            }
        }
    }
    $per_page_html .= "</div>";
}

$query = $sql.$limit;
$pdo_statement = $connection->prepare($query);
$pdo_statement->execute();
$product = $pdo_statement->fetchAll();
//end pagination part

foreach ($product as $productLol){
    $delete="delete".$productLol['id'];
    if (!empty($_POST[$delete])){
        $imgSrc = '../'.$productLol['pageImg'];
        $connection->query("DELETE FROM catalog where id='$productLol[id]'");
        unlink($imgSrc);
        header('Location:catalog.php');
    }
}
$goodExtension = ['jpg','jpeg','png'];

if (!empty($_POST['tovarName'])){
    $newProduct = $_POST['tovarName'];
    $productCategory = $_POST['tovarCategory'];
    $productDescr = htmlspecialchars_decode($_POST['tovarDescr']);

    $nameForBd = 'images/default.png';

    $fileName = $_FILES['tovarFile']['name'];
    $fileTmpName = $_FILES['tovarFile']['tmp_name'];
    $fileType = $_FILES['tovarFile']['type'];
    $fileError = $_FILES['tovarFile']['error'];
    $fileSize = $_FILES['tovarFile']['size'];
    $fileExtension = strtolower(end(explode('.', $fileName)));

        if (in_array($fileExtension,$goodExtension)){
            if ($fileSize<500000){
                if ($fileError==0){
                    $fileNameNew = time().$fileName;
                    $nameForBd = 'images/'.$fileNameNew;
                    $connection->query("INSERT INTO catalog (name, image, content) VALUE ('$newProduct','$nameForBd','$productDescr')");
                    $fileDestination = '../images/'.$fileNameNew;
                    move_uploaded_file($fileTmpName,$fileDestination);
                }
                else{
                    echo 'Что-то пошло не так';
                }
            }
            else{
                echo 'Слишком большой файл';
            }
        } else{
            echo 'Неверный тип файла';
        }

    header('Location:catalog.php');
}
?>

<body class="nav-md">

  <div class="container body">


    <div class="main_container">

        <?php
        require "sidebar.php";
        require "navigation.php";
        ?>


      <!-- page content -->
      <div class="right_col" role="main">
          <div class="container newpage">
              <div class="row add_page">
                  <h1 class="text-center">New catalog</h1>
                  <form class="registration-form" method="POST" enctype="multipart/form-data">
                      <label>
                          <span class="label-text">Имя</span>
                          <input type="text" name="tovarName" required>
                      </label>
                      <label>
                          <span class="label-text">Картинка</span>
                          <input type="file" name="tovarFile" required>
                      </label>
                      <label>
                          <span class="label-text">Описание</span>
                          <textarea style="width: 100% !important;" name="tovarDescr" id="" cols="80" rows="10" required placeholder="Описание"></textarea>
                      </label>
                      <div class="text-center">
                          <button class="submit" name="submit">Добавить</button>
                      </div>
                  </form>
              </div>
          </div>
                <div class="row mainAdmin" style="width: 100%">
                    <?php
                    foreach ($product as $productLol){
                    ?>
                        <div class="col-md-4">
                            <div class="product-cart">
                                <h3> <?=$productLol['name']?> </h3>
                                <img src="../<?=$productLol['image']?>" alt="">
                                <form method="POST">
                                    <input value="УДАЛИТЬ" type="submit" name="delete<?=$productLol['id']?>">
                                </form>
<!--                                <a href="modifyCatalog.php?tovar=--><?//=$productLol['id']?><!--"><input type="button" value="Редактировать"></a>-->
                            </div>
                        </div>
                    <?php
                        }
                    ?>

                </div>

      </div>
            <?php echo $per_page_html; ?>
      <!-- /page content -->

    </div>

  </div>
<?php
require "footer.php";
