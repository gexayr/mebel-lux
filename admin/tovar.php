<?php
require "../pdo/common.php";
require "../pdo/config.php";
require "header.php";

define("ROW_PER_PAGE",30);

$pages = $connection->query("SELECT * FROM potolkiv_mebel.mainPage");

//pagination_part
if (isset($_GET['pageno'])) {
    $pageno = $_GET['pageno'];
} else {
    $pageno = 1;
}
/* Pagination Code starts */
$sql = 'SELECT * FROM product ORDER BY id DESC ';
$per_page_html = '';
$page = 1;
$start=0;
if(!empty($_GET["page_number"])) {
    $page = $_GET["page_number"];
    $start=($page-1) * ROW_PER_PAGE;
}


$limit=" limit " . $start . "," . ROW_PER_PAGE;
$pagination_statement = $connection->prepare($sql);
$pagination_statement->execute();

$row_count = $pagination_statement->rowCount();
if(!empty($row_count)){
    $per_page_html .= "<div style='text-align:center;margin:20px 0px;'>";
    $page_count=ceil($row_count/ROW_PER_PAGE);
    if($page_count>1) {
        for($i=1;$i<=$page_count;$i++){
            if($i==$page){
                $per_page_html .= '<span class="btn-page current btn-dark">' . $i . '</span>';
            } else {
                $per_page_html .= '<a href="?page_number=' . $i . '" class="btn-page">' . $i . '</a>';
            }
        }
    }
    $per_page_html .= "</div>";
}

$query = $sql.$limit;
$pdo_statement = $connection->prepare($query);
$pdo_statement->execute();
$product = $pdo_statement->fetchAll();
//end pagination part

foreach ($product as $productLol){
    $delete="delete".$productLol['id'];
    if (!empty($_POST[$delete])){
        $imgSrc = '../'.$productLol['pageImg'];
        $connection->query("DELETE FROM product where id='$productLol[id]'");
        unlink($imgSrc);
        header('Location:tovar.php');
    }
}
$goodExtension = ['jpg','jpeg','png'];

if (!empty($_POST['tovarName'])){
    $newProduct = $_POST['tovarName'];
    $productCategory = $_POST['tovarCategory'];
    $productDescr = htmlspecialchars_decode($_POST['tovarDescr']);
    $productPrice = $_POST['tovarPrice'];
    $productStock = $_POST['stock'] ? $_POST['stock'] : 0;

    $nameForBd = 'images/default.png';
    $connection->query("INSERT INTO potolkiv_mebel.product 
        (productName, productImg, productDescr,price,category, stock) VALUE 
        ('$newProduct','$nameForBd','$productDescr','$productPrice','$productCategory','$productStock')");
    $productId = $connection->lastInsertId();

    foreach ($_FILES['tovarFile']['name'] as $key=>$value) {
        $fileName = $value;
        $fileTmpName = $_FILES['tovarFile']['tmp_name'][$key];
        $fileType = $_FILES['tovarFile']['type'][$key];
        $fileError = $_FILES['tovarFile']['error'][$key];
        $fileSize = $_FILES['tovarFile']['size'][$key];
        $fileExtension = strtolower(end(explode('.', $fileName)));

        if (in_array($fileExtension,$goodExtension)){
            if ($fileSize<10000000){
                if ($fileError==0){
                    $fileNameNew = time().$fileName;
                    $nameForBd = 'images/'.$fileNameNew;
                    $connection->query("INSERT INTO product_images (name, product_id) VALUE ('$nameForBd','$productId')");
                    $fileDestination = '../images/'.$fileNameNew;
                    $resizedFileDestination = '../resized_images/'.$fileNameNew;
                    copy($fileTmpName,$resizedFileDestination);
                    move_uploaded_file($fileTmpName,$fileDestination);
                }
                else{
                    echo 'Что-то пошло не так';
                }
            }
            else{
                echo 'Слишком большой файл';
            }
        } else{
            echo 'Неверный тип файла';
        }
    }

    header('Location:tovar.php');
}
?>

<body class="nav-md">

  <div class="container body">


    <div class="main_container">

        <?php
        require "sidebar.php";
        require "navigation.php";
        ?>


      <!-- page content -->
      <div class="right_col" role="main">
          <div class="container newpage">
              <div class="row add_page">
                  <h1 class="text-center">New product</h1>
                  <form class="registration-form" method="POST" enctype="multipart/form-data">
                      <label class="col-one-half">
                          <span class="label-text">Имя товара</span>
                          <input type="text" name="tovarName" required>
                      </label>
                      <label class="col-one-half">
                          <span class="label-text">Цена товара</span>
                          <input type="text" name="tovarPrice" required>
                      </label>
                      <label>
                          <span class="label-text">Картинка товара</span>
                          <input type="file" name="tovarFile[]" required multiple>
                      </label>
                      <label>
                          <span class="label-text">Категория</span>
                          <select name="tovarCategory" id="" required>
                              <?foreach ($pages as $pagesLol){?>
                                  <option value="<?=$pagesLol['name']?>"><?=$pagesLol['name']?></option>
                              <?}?>
                          </select>
                      </label>
                      <label>
                          <span class="span-akcia">Товар по акции</span>
                          <input type="checkbox" name="stock" class="checkbox-akcia" value="1">
                      </label>
                      <label>
                          <span class="label-text">Описание товара</span>
                          <textarea style="width: 100% !important;" name="tovarDescr" id="" cols="80" rows="10" required placeholder="Описание товара"></textarea>
                      </label>
                      <div class="text-center">
                          <button class="submit" name="submit">Добавить</button>
                      </div>
                  </form>
              </div>
          </div>
                <div class="row mainAdmin">
                    <?php
                    foreach ($product as $productLol){
                        $sql = "SELECT * FROM product_images
        where product_id = :id";
                        $statement = $connection->prepare($sql);
                        $statement->bindParam(':id', $productLol['id'], PDO::PARAM_INT);
                        $statement->execute();
                        $result = $statement->fetchAll();

                        $productImg = $result;
                        if(!empty($productImg)) {
                            $productLol['productImg'] = $productImg[0]['name'];
                        }
                    ?>
                        <div class="col-md-4">
                            <div class="product-cart">
                                <h3> <?=$productLol['productName']?> </h3>
                                <?php
                                   $prefix = '';
                                   if(strpos($productLol['productImg'], 'http') === false){
                                       $prefix = "../";
                                   }
                                ?>
                                <?php if(!empty($productLol['stock'])) { ?>
                                    <img src="../images/akcia1.png" class="img-akcia" alt="Colorlib Template">
                                <?php } ?>
                                <img src="<?=$prefix . $productLol['productImg']?>" alt="">
                                <p>Категория: <?=$productLol['category']?></p>
                                <form method="POST">
                                    <input value="УДАЛИТЬ" type="submit" name="delete<?=$productLol['id']?>">
                                </form>
                                <a href="modifyTovar.php?tovar=<?=$productLol['id']?>"><input type="button" value="Редактировать"></a>
                            </div>
                        </div>
                    <?php
                        }
                    ?>

                </div>

      </div>
            <?php echo $per_page_html; ?>
      <!-- /page content -->

    </div>

  </div>
<?php
require "footer.php";
