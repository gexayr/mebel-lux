<?php
require "../pdo/common.php";
require "../pdo/config.php";
require "header.php";
define("ROW_PER_PAGE",30);

//pagination_part
if (isset($_GET['pageno'])) {
    $pageno = $_GET['pageno'];
} else {
    $pageno = 1;
}
/* Pagination Code starts */
$sql = 'SELECT * FROM potolkiv_mebel.mainPage ORDER BY id DESC ';
$per_page_html = '';
$page = 1;
$start=0;
if(!empty($_GET["page_number"])) {
    $page = $_GET["page_number"];
    $start=($page-1) * ROW_PER_PAGE;
}

$limit=" limit " . $start . "," . ROW_PER_PAGE;
$pagination_statement = $connection->prepare($sql);
$pagination_statement->execute();

$row_count = $pagination_statement->rowCount();
if(!empty($row_count)){
    $per_page_html .= "<div style='text-align:center;margin:20px 0px;'>";
    $page_count=ceil($row_count/ROW_PER_PAGE);
    if($page_count>1) {
        for($i=1;$i<=$page_count;$i++){
            if($i==$page){
                $per_page_html .= '<span class="btn-page current btn-dark">' . $i . '</span>';
            } else {
                $per_page_html .= '<a href="?page_number=' . $i . '" class="btn-page">' . $i . '</a>';
            }
        }
    }
    $per_page_html .= "</div>";
}

$query = $sql.$limit;
$pdo_statement = $connection->prepare($query);
$pdo_statement->execute();
$product = $pdo_statement->fetchAll();
//end pagination part

$pages = $connection->query("SELECT * FROM potolkiv_mebel.mainPage");
$pages = $pages->fetchAll();
foreach ($pages as $pagesLol){
   $delete="delete".$pagesLol['id'];
    if (!empty($_POST[$delete])){
        $imgSrc = 'images/category/'.$pagesLol['pageImg'];
        $connection->query("DELETE FROM potolkiv_mebel.mainPage where id='$pagesLol[id]'");
        unlink($imgSrc);
        header('Location:category.php');
   }
}

if (isset($_POST['submit'])){
    $categoryName = $_POST['firstName'];
    $categoryNumber = $_POST['lastName'];
    $fileName = $_FILES['file']['name'];
    $fileTmpName = $_FILES['file']['tmp_name'];
    $fileType = $_FILES['file']['type'];
    $fileError = $_FILES['file']['error'];
    $fileSize = $_FILES['file']['size'];

    $fileExtension = strtolower(end(explode('.', $fileName)));

    $goodExtension = ['jpg','jpeg','png'];

    if (in_array($fileExtension,$goodExtension)){
        if ($fileSize<500000){
            if ($fileError==0){
                $connection->query("INSERT INTO mainPage (name,number,pageImg) VALUE ('$categoryName','$categoryNumber','$fileName')");
                $fileDestination = 'images/category/'.$fileName;
                move_uploaded_file($fileTmpName,$fileDestination);
            }
            else{
                echo 'Что-то пошло не так';
            }
        }
        else{
            echo 'Слишком большой файл';
        }
    } else{
        echo 'Неверный тип файла';
    }
    header('Location:category.php');
}
?>

<body class="nav-md">

  <div class="container body">


    <div class="main_container">

        <?php
            require "sidebar.php";
            require "navigation.php";
        ?>

      <!-- page content -->
      <div class="right_col" role="main">
            <div class="row mainAdmin">
                <?foreach ($pages as $pagesLol){?>
                <div class="col-md-4">
                    <div class="product-cart">
                        <h3> <?=$pagesLol['name']?> </h3>
                        <img src="images/category/<?=$pagesLol['pageImg']?>" alt="">
                        <p>Номер категории: <?=$pagesLol['number']?></p>
                        <form method="POST">
                            <input value="УДАЛИТЬ" type="submit" name="delete<?=$pagesLol['id']?>">
                        </form>
                    </div>
                </div>
                <?}?>
            </div>
          <div class="container newpage">
          <div class="row add_page">
              <h1 class="text-center">New category</h1>
              <form class="registration-form" method="POST" enctype="multipart/form-data">
                  <label class="col-one-half">
                      <span class="label-text">Имя категории</span>
                      <input type="text" name="firstName" required>
                  </label>
                  <label class="col-one-half">
                      <span class="label-text">Номер категории</span>
                      <input type="text" name="lastName" required>
                  </label>
                  <label>
                      <span class="label-text">Картинка для категории</span>
                      <input type="file" name="file" required>
                  </label>
                  <div class="text-center">
                      <button class="submit" name="submit">Добавить</button>
                  </div>
              </form>
          </div>
          </div>
      </div>

      <!-- /page content -->

    </div>

  </div>

<?php
require "footer.php";