<?php
ob_start();
require "../pdo/common.php";
require "../pdo/config.php";
require "header.php";

if (!$_SESSION['login'] || !$_SESSION['pass']){
    header('Location:login.php');
    die();
}

if(isset($_POST['submit'])){
    session_destroy();
    header('Location:../index.php');
}
?>

<body class="nav-md">
  <div class="container body">
    <div class="main_container">
        <?php
        require "sidebar.php";
        require "navigation.php";
        ?>

      <!-- page content -->
      <div class="right_col" role="main">
            <div class="row mainAdmin">
                <div class="col-sm-3">
                    <h3>Категории</h3>
                    <img src="images/spisok.jpg" alt="">
                    <a href="category.php"><button>ПЕРЕЙТИ</button></a>
                </div>
                <div class="col-sm-3">
                    <h3>Товары</h3>
                    <img src="images/tovar.png" alt="">
                    <a href="tovar.php"><button>ПЕРЕЙТИ</button></a>
                </div>
                <div class="col-sm-3">
                    <h3>Галерея</h3>
                    <img src="images/galery.jpg" alt="">
                    <button>ПЕРЕЙТИ</button>
                </div>
            </div>
      </div>
      <!-- /page content -->

    </div>

  </div>

  <div id="custom_notifications" class="custom-notifications dsp_none">
    <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
    </ul>
    <div class="clearfix"></div>
    <div id="notif-group" class="tabbed_notifications"></div>
  </div>
<?php
require "footer.php";

