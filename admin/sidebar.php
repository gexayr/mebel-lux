<div class="col-md-3 left_col">
    <div class="left_col scroll-view">

        <div class="navbar nav_title" style="border: 0;">
            <a href="index.php" class="site_title"><i class="fa fa-paw"></i> <span>Hi, Admin</span></a>
        </div>
        <div class="clearfix"></div>

        <!-- menu prile quick info -->
        <div class="profile">
            <div class="profile_pic">
                <img src="images/user.png" alt="..." class="img-circle profile_img">
            </div>
            <div class="profile_info">
                <span>Welcome,</span>
                <h2>Stas</h2>
            </div>
        </div>
        <!-- /menu prile quick info -->

        <br />

        <!-- sidebar menu -->
        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

            <div class="menu_section">
                <h3>General</h3>
                <ul class="nav side-menu">
                    <li>
                        <a href="index.php"><i class="fa fa-home"></i> Главная </a>
                    </li>
                    <li>
                        <a href="category.php"><i class="fa fa-edit"></i> Категории </a>
                    </li>
                    <li>
                        <a href="tovar.php"><i class="fa fa-desktop"></i> Товары</a>
                    </li>
                    <li>
                        <a href="catalog.php"><i class="fa fa-table"></i> Каталог </a>
                    </li>
                    <li>
                        <a href="../index.php"><i class="fa fa-laptop"></i> Mebellux <span class="label label-success pull-right">Вернуться</span></a>
                    </li>
                </ul>
            </div>
        </div>
        <!-- /sidebar menu -->

        <!-- /menu footer buttons -->
        <div class="sidebar-footer hidden-small">
            <a data-toggle="tooltip" data-placement="top" title="Выйти">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
            </a>
        </div>
        <!-- /menu footer buttons -->
    </div>
</div>