<nav id="colorlib-main-nav" class="border" role="navigation">
    <a href="#" class="js-colorlib-nav-toggle colorlib-nav-toggle active"><i></i></a>
    <div class="js-fullheight colorlib-table">
        <div class="img" style="background-image: url(resized_images/bg_2.jpg);"></div>
        <div class="colorlib-table-cell js-fullheight">
            <div class="row no-gutters">
                <div class="col-md-12 text-center">
                    <h1 class="mb-4"><a href="index.php" class="logo">
                            <img src="images/logo/1.png" alt="ХочуМебельLux" width="80">
                        </a></h1>
                    <ul>
                        <li class="active"><a href="index.php"><span>Главная</span></a></li>
                        <li><a href="contact.php"><span>Контакты</span></a></li>
                        <?foreach ($pages as $pagesLol){?>
                            <li><a href="catalog.php?page=<?=$pagesLol['number']?>&num=1"><span><?=$pagesLol['name']?></span></a></li>
                        <?}?>
                        <li><a href="tables.pdf" target="_blank"><span> Каталог столов</span></a></li>
                        <li><a href="Binder1.pdf" target="_blank"><span>Открыть каталог</span></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</nav>